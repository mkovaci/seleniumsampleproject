package selenium.test1.selenium;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Test1 {
	FirefoxDriver driver;
	JavascriptExecutor js;

	@Before
	public void setUp() {
//		System.setProperty("webdriver.gecko.driver", "C:\\dev\\geckodriver-v0.24.0-win64\\geckodriver.exe");
		driver = new FirefoxDriver();
		js = (JavascriptExecutor) driver;
	}

	@After
	public void tearDown() {
		driver.quit();
	}

	@org.junit.Test
	public void test1() {
		driver.get("http://www.nox-nachtexpress.de");
		driver.manage().window().setSize(new Dimension(1076, 787));

		String teaserBoxContent = driver.findElement(By.className("teaser__box-content")).getText();
		System.out.println("Found: " + teaserBoxContent);
		assertThat(teaserBoxContent, startsWith("NOX NACHTEXPRESS"));

		driver.findElement(By.cssSelector("#menu-item-83 > a")).click();

		String subHeadline = driver.findElement(By.cssSelector("h1 > strong")).getText();
		System.out.println("Found: " + subHeadline);
		assertThat(subHeadline, startsWith("After Sales Lösungen für jede Anforderung"));

	}

}
