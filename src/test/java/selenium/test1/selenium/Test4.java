package selenium.test1.selenium;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Test4 {
	FirefoxDriver driver;
	JavascriptExecutor js;

	@Before
	public void setUp() {
//		System.setProperty("webdriver.gecko.driver", "C:\\dev\\geckodriver-v0.24.0-win64\\geckodriver.exe");
		driver = new FirefoxDriver();
		js = (JavascriptExecutor) driver;
	}

	@After
	public void tearDown() {
		driver.quit();
	}

	@org.junit.Test
	public void test1() {
		driver.get("http://www.wetest.de");
	    driver.manage().window().setSize(new Dimension(1076, 788));
	    driver.findElement(By.linkText("WeTest")).click();

		String teaserBoxContent = driver.findElement(By.cssSelector("h1")).getText();
		System.out.println("Found: " + teaserBoxContent);
		assertThat(teaserBoxContent, notNullValue());

		

	}

}
